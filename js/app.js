// markers = [
// 	{
// 		name: "Marker 1"
// 	},
// 	{
// 		name: "Marker 2"
// 	},
// 	{
// 		name: "Marker 3"
// 	},
// 	{
// 		name: "Marker 4"
// 	},
// 	{
// 		name: "Marker 5"
// 	}
// ]


var Marker = function(data) {

	this.name = data.name;
	this.pos = {lat: data.lat, lng: data.lng};
	this.information = data.information;

};


var Map = function(data) {

	var self = this;
	this.viewModel = data.self;
	var myLatlng = new google.maps.LatLng(45.5511100,18.6938900);

  var mapOptions = {
    center: myLatlng,
    zoom: 8
  };

  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  if (this.viewModel.markerList().length > 0) {
  	this.viewModel.markerList().forEach(function (marker) {
  		var marker = new google.maps.Marker({
			  position: new google.maps.LatLng(marker.pos.lat, marker.pos.lng),
			  map: map,
			  title: marker.name,
			  draggable: true
			});
  	});
  }

	google.maps.event.addListener(map, 'click', function(event) {
		var create = confirm("Create marker here?");
		if (create) {
			var marker = new google.maps.Marker({
			  position: event.latLng,
			  map: map,
			  title: 'Hello World!',
			  draggable: true
			});
			google.maps.event.addListener(marker, 'click', function() {
		    map.setZoom(8);
		    map.setCenter(marker.getPosition());
		  });
		  // TODO add marker id number to icon
		  var markerName = prompt("Please enter marker name", "");
			if (markerName != null) {
				self.viewModel.addMarker({
										name: markerName,
										lat: event.latLng.A,
										lng: event.latLng.F,
										information: ""});
			}
		}
	});

}


var ViewModel = function() {

	var self = this;
	this.markerList = (function() {
		if (localStorage.markers) {
			return ko.observableArray(JSON.parse(localStorage.markers));
		} else {
			return ko.observableArray([]);
		}
	})();

	this.map = new Map({self: this});

	if (this.markerList().length > 0) {
		this.currentMarker = ko.observable(0);
	} else {
			this.currentMarker = ko.observable(this.markerList()[0]);
	};

	this.updateCurrentMarker = function(marker) {
		return self.currentMarker(marker);
	};

	this.addMarker = function(marker) {
		self.markerList.push(new Marker(marker));
		localStorage.markers = JSON.stringify(self.markerList());
	};

	this.saveMarkerInformation = function() {
		console.log(this);
		this.information = document.getElementById('marker-information').value;
		localStorage.markers = JSON.stringify(self.markerList());
	}
	// attendance = JSON.parse(localStorage.attendance);
}

ko.applyBindings(new ViewModel());